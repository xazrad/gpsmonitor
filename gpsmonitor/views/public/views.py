# _*_ coding: utf-8_*_

__author__ = 'radik'

from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


class PublicView(TemplateView):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('login'))
