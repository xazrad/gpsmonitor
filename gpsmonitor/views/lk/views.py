# _*_ coding: utf-8_*_

__author__ = 'radik'

from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


class DashboardView(TemplateView):
    template_name = 'gpsmonitor/lk/dashboard.html'
