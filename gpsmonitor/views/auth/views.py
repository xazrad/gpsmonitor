# _*_ coding: utf-8 _*_

__author__ = 'radik'

from django.core.urlresolvers import reverse
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.core.urlresolvers import reverse_lazy

from .forms import LoginForm


class BaseAuthForm(FormView):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('dashboard'))
        return super(BaseAuthForm, self).get(request, *args, **kwargs)


class LoginView(BaseAuthForm):
    template_name = 'gpsmonitor/auth/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('dashboard')

    def form_valid(self, form):
        auth_login(self.request, form.user)
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            self.success_url = next_url
        return super(LoginView, self).get_success_url()
