# _*_ coding: utf-8 _*_

__author__ = 'radik'

from django.contrib.auth import authenticate
from django import forms


class LoginForm(forms.Form):
    email = forms.EmailField(max_length=30)
    password = forms.CharField()

    def clean_password(self):
        self.password = self.cleaned_data['password']
        self.email = self.cleaned_data.get('email')
        if not self.email:
            return

        self.user = authenticate(username=self.email, password=self.password)
        if self.user is None:
            raise forms.ValidationError(u'Неверные данные')
